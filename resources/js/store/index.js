import { createStore } from "vuex";
import auth from "@/store/auth";
import createPersistedState from "vuex-persistedstate";

const store = createStore({
    plugins: [createPersistedState()],
    modules: {
        auth,
    },
});

export default store;
